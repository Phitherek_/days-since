class JsonWebToken
  class << self
    def encode(payload, expiry = 24.hours.from_now)
      payload[:exp] = expiry.to_i
      JWT.encode(payload, Rails.application.credentials[:secret_key_base])
    end

    def decode(token)
      body = JWT.decode(token, Rails.application.credentials[:secret_key_base])
      HashWithIndifferentAccess.new(body[0])
    rescue
      nil
    end
  end
end