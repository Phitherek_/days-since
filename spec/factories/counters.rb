FactoryBot.define do
  factory :counter do
    association :user
    sequence(:name) { |n| "Test Counter #{n}" }
    sequence(:slug) { |n| "test#{n}" }
    sequence(:description) { |n| "test event #{n}" }
    last_date { Time.current }
    public { false }
  end
end
