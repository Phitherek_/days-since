FactoryBot.define do
  factory :user do
    sequence(:name) { |n| "TestUsername#{n}" }
    password { 'password' }
    password_confirmation { 'password' }
  end
end
