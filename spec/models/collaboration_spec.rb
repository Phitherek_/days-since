require 'rails_helper'

RSpec.describe Collaboration, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:counter) }
  it { is_expected.to validate_uniqueness_of(:counter_id).scoped_to(:user_id) }
end
