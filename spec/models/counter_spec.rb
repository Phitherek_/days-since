require 'rails_helper'

RSpec.describe Counter, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:collaborations).dependent(:destroy) }
  it { is_expected.to have_many(:collaborators).through(:collaborations).source(:user) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to callback(:setup_slug).before(:validation) }
  it { is_expected.to callback(:send_new_values).after(:save) }
  describe '#setup_slug' do
    it 'generates proper slug' do
      counter = build(:counter)
      counter.name = 'Test Name'
      counter.slug = nil
      expect(counter.save).to be_truthy
      expect(counter.slug).to eq('test-name')
    end
  end

  describe '#current_value' do
    let!(:time) { Time.current }
    before do
      allow(Time).to receive(:current).and_return(time)
    end
    it 'returns number of days from last_date' do
      counter1 = create(:counter, last_date: time)
      counter2 = create(:counter, last_date: time + 1.second)
      counter3 = create(:counter, last_date: time - 1.day + 1.second)
      counter4 = create(:counter, last_date: time - 1.day)
      expect(counter1.current_value).to eq(0)
      expect(counter2.current_value).to eq(-1)
      expect(counter3.current_value).to eq(0)
      expect(counter4.current_value).to eq(1)
    end
  end

  describe '#reset' do
    let!(:time) { Time.current }
    before do
      allow(Time).to receive(:current).and_return(time)
    end
    it 'resets last_date to current time' do
      counter = create(:counter, last_date: time - 1.hour)
      expect(counter.reset).to be_truthy
      expect(counter.last_date).to eq(time)
    end
  end
end
