require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_secure_password }
  it { is_expected.to have_many(:counters).dependent(:destroy) }
  it { is_expected.to have_many(:collaborations).dependent(:destroy) }
  it { is_expected.to have_many(:collaborator_counters).through(:collaborations).source(:counter) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name) }
end
