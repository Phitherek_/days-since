const webpack = require('webpack');

const { environment } = require('@rails/webpacker');

environment.loaders.get('sass').use.splice(-1, 0, {
  loader: 'resolve-url-loader'
});

environment.plugins.append(
  'ProvidePlugin',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    moment: 'moment'
  })
);

module.exports = environment;
