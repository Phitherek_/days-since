Rails.application.routes.draw do
  root to: 'home#index'

  resources :users, only: [:new, :create] do
    collection do
      get :login
      post :do_login
      delete :logout
    end
  end

  resource :user, only: [:edit, :update, :destroy]
  resources :counters do
    member do
      get :fullscreen
      post :reset
    end
    resources :collaborations, only: [:index, :create, :destroy]
  end

  namespace :api do
    resources :users, only: [:none] do
      collection do
        post :token
      end
    end

    resources :counters, only: [:index, :show] do
      member do
        post :reset
      end
    end
  end
end
