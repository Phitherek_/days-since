# Days Since API Documentation

This is the documentation for Days Since REST API.

## Common responses

### Authorization error (auth)

The API uses JWT token-based user authorization. If it fails, the following error response is returned:

`Status: 401`
```json
{ "error": "Missing token/Invalid token" }
```

### Resource not found error (notfound)

If a resource cannot be found, the following error response is returned:

`Status: 404`
```json
{ "error": "Resource not found" }
```

### Unprocessable entity error (unprocessable)

If an action on a resource cannot be completed, the following error response is returned:

`Status: 422`
```json
{ "error": "Could not perform action", "messages": ["Field is invalid", "Field2 is invalid"] }
```

## Users

### `POST /api/users/token`

Get API token for user

* Request: `application/json`
```json
{ "user": { "name": "Username", "password": "Password" } }
```
* Response: 
    * Success:
    
    `Status: 200`
    ```json
    { "token": "jwttokencontent" } 
    ```
    * Invalid username or password:
    
    `Status: 401`
    ```json
    { "error": "Invalid username or password" }
    ```

## Counters

### `GET /api/counters` (Possible errors: auth)

Get all user counters

* Request:
    * Headers: `Authorization: Token jwttokencontent` (from `/api/users/token` response)
* Response:
    * Success:
    
    `Status: 200`
    ```json
    {
        "counters": [
            {
                "slug": "service-start",
                "created_at": "2020-02-23 00:52:01 UTC",
                "current_value": 67,
                "description": "the start of this service",
                "full_description": "days since the start of this service",
                "last_date": "2020-02-23 01:00:00 UTC",
                "name": "Service start",
                "public": true,
                "record_value": 67,
                "updated_at": "2020-02-23 01:01:35 UTC"
            },
            {
                "slug": "another-counter",
                "created_at": "2020-02-23 00:52:01 UTC",
                "current_value": 123,
                "description": "something",
                "full_description": "days since something",
                "last_date": "2020-02-23 01:00:00 UTC",
                "name": "Another counter",
                "public": false,
                "record_value": 234,
                "updated_at": "2020-02-23 01:01:35 UTC"
            }
        ]
    }  
    ```
### `GET /api/counters/{slug}` (Possible errors: notfound)

Get specific counter that is public or accessible by logged in user

* Request:
    * Path parameters: `slug` - slug of the counter
    * Headers: `Authorization: Token jwttokencontent` (from `/api/users/token` response) (OPTIONAL)
* Response:
    * Success:
    
    `Status: 200`
    ```json
    {
        "counter": {
            "slug": "service-start",
            "created_at": "2020-02-23 00:52:01 UTC",
            "current_value": 67,
            "description": "the start of this service",
            "full_description": "days since the start of this service",
            "last_date": "2020-02-23 01:00:00 UTC",
            "name": "Service start",
            "public": true,
            "record_value": 67,
            "updated_at": "2020-02-23 01:01:35 UTC"
        }
    }  
    ```
### `POST /api/counters/{slug}/reset` (Possible errors: auth, notfound, unprocessable)

Reset counter to current datetime

* Request:
    * Path parameters: `slug` - slug of the counter
    * Headers: `Authorization: Token jwttokencontent` (from `/api/users/token` response)
* Response:
    * Success:
    
    `Status: 200`
    ```json
    {
        "counter": {
            "slug": "service-start",
            "created_at": "2020-02-23 00:52:01 UTC",
            "current_value": 0,
            "description": "the start of this service",
            "full_description": "days since the start of this service",
            "last_date": "2020-02-23 01:00:00 UTC",
            "name": "Service start",
            "public": true,
            "record_value": 67,
            "updated_at": "2020-02-23 01:01:35 UTC"
        }
    }  
    ```