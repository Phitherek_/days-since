class AddRecordValueToCounters < ActiveRecord::Migration[6.0]
  def change
    add_column :counters, :record_value, :integer
  end
end
