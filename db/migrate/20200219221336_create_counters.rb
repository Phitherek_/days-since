class CreateCounters < ActiveRecord::Migration[6.0]
  def change
    create_table :counters do |t|
      t.belongs_to :user
      t.string :name
      t.string :slug
      t.string :description
      t.timestamp :last_date
      t.boolean :public, default: false
      t.timestamps
    end
  end
end
