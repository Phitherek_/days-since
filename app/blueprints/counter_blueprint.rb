class CounterBlueprint < Blueprinter::Base
  identifier :slug

  fields :name, :description, :current_value, :record_value, :full_description, :last_date, :public, :created_at, :updated_at
end