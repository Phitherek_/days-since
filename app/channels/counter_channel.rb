class CounterChannel < ApplicationCable::Channel
  def subscribed
    @counter = Counter.find(params[:id])
    stream_for @counter
  end

  def self.update_action_params(counter)
    {
      action: :update,
      value: counter.current_value,
      record_value: counter.record_value,
      description: counter.full_description
    }
  end

  periodically every: 5.minutes do
    transmit self.class.update_action_params(@counter)
  end
end