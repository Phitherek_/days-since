module ApplicationCable
  class Channel < ActionCable::Channel::Base
    def self.pluralize_without_count(count, singular)
      case count
      when 1, -1
        singular
      else
        singular.pluralize
      end
    end
  end
end
