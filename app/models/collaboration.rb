class Collaboration < ApplicationRecord
  belongs_to :user
  belongs_to :counter
  validates :counter_id, uniqueness: { scope: :user_id }
end
