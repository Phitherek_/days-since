class Counter < ApplicationRecord
  belongs_to :user
  has_many :collaborations, dependent: :destroy
  has_many :collaborators, through: :collaborations, source: :user

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true
  validates :description, presence: true

  before_validation :setup_slug
  after_save :send_new_values

  def current_value
    if @current_value.blank?
      @current_value = ((Time.current - last_date) / 1.day).floor.to_i
      update_record_value(@current_value)
    end
    @current_value
  end

  def reset
    self.last_date = Time.current
    save
  end

  def full_description
    "#{pluralize_without_count(current_value, 'day')} since #{description}"
  end

  private

  def update_record_value(current_value)
    update(record_value: current_value) if record_value.blank? || current_value > record_value
  end

  def setup_slug
    self.slug = (slug.presence || name)&.parameterize
  end

  def send_new_values
    CounterChannel.broadcast_to(self, CounterChannel.update_action_params(self))
  end

  def pluralize_without_count(count, singular)
    case count
    when 1, -1
      singular
    else
      singular.pluralize
    end
  end
end
