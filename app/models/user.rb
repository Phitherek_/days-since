class User < ApplicationRecord
  has_secure_password
  has_many :counters, dependent: :destroy
  has_many :collaborations, dependent: :destroy
  has_many :collaborator_counters, through: :collaborations, source: :counter
  validates :name, presence: true, uniqueness: true
end
