class Api::CountersController < Api::BaseController
  skip_before_action :authenticate_user, only: :show
  def index
    @counters = Counter
                 .includes(collaborations: :user)
                 .where('counters.user_id = ? OR collaborations.user_id = ?', current_user&.id, current_user&.id)
                 .references(:counters, :collaborations)
    render json: CounterBlueprint.render(@counters, { root: :counters })
  end

  def show
    find_counter_for_display
    render json: { error: "Counter not found: #{params[:id]}" }, status: 404 and return if @counter.blank?

    render json: CounterBlueprint.render(@counter, { root: :counter })
  end

  def reset
    find_counter
    render json: { error: "Counter not found: #{params[:id]}" }, status: 404 and return if @counter.blank?

    if @counter.reset
      render json: CounterBlueprint.render(@counter, { root: :counter })
    else
      render json: { error: "Could not reset counter", messages: @counter.errors.full_messages }, status: 422
    end
  end

  private

  def find_counter
    @counter = Counter
                 .includes(collaborations: :user)
                 .where('counters.user_id = ? OR collaborations.user_id = ?', current_user&.id, current_user&.id)
                 .references(:counters, :collaborations)
                 .find_by(slug: params[:id])
  end

  def find_counter_for_display
    @counter = Counter
                 .includes(:collaborations)
                 .where('counters.user_id = ? OR collaborations.user_id = ? OR public IS TRUE', current_user&.id, current_user&.id)
                 .references(:counters, :collaborations)
                 .find_by(slug: params[:id])
  end
end
