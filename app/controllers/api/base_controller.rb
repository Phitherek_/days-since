class Api::BaseController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :authenticate_user

  def current_user
    @current_user ||= User.find_by(id: decoded_auth_token[:user_id]) if decoded_auth_token
    @current_user
  end

  def authenticate_user
    render json: { error: 'Missing token' }, status: 401 and return if http_auth_header.blank?
    render json: { error: 'Invalid token' }, status: 401 if current_user.blank?
  end

  def decoded_auth_token
    return @decoded_auth_token if http_auth_header.blank?
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    if request.headers['Authorization'].present?
      return request.headers['Authorization'].split(' ').last
    end
    nil
  end
end
