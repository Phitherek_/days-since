class Api::UsersController < Api::BaseController
  skip_before_action :authenticate_user, only: :token

  def token
    @user = User.find_by(name: params[:user][:name])&.authenticate(params[:user][:password])
    render json: { error: 'Invalid username or password' }, status: 401 and return if @user.blank?

    render json: { token: JsonWebToken.encode(user_id: @user.id) }
  end
end