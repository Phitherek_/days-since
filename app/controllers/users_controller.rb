class UsersController < ApplicationController
  before_action :require_current_user, only: [:edit, :update, :destroy, :logout]

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.persisted?
      flash[:success] = 'Your user account was created successfully'
      redirect_to root_url
    else
      flash[:error] = "Failed to create your user account: #{@user.errors.full_messages.join(', ')}"
      render :new
    end
  end

  def edit; end

  def update
    if current_user.authenticate(params[:user][:old_password])
      if current_user.update(user_params)
        flash[:success] = 'Your information has been updated successfully'
      else
        flash[:error] = "Failed to update your information: #{current_user.errors.full_messages.join(', ')}}"
      end
    else
      flash[:error] = "Failed to update your information: Old password is invalid"
    end
    render :edit
  end

  def destroy
    if current_user.destroy
      reset_session
      flash[:success] = 'Your user account has been deleted successfully'
    else
      flash[:error] = 'Failed to delete your user account'
    end
    redirect_to root_url
  end

  def login
    @user = User.new
  end

  def do_login
    @user = User.find_by(name: params[:user][:name])&.authenticate(params[:user][:password])
    if @user
      session[:current_user_id] = @user.id
      flash[:success] = 'Login successful'
      redirect_to root_url
    else
      flash[:error] = 'Login failed: Wrong username or password'
      render :login
    end
  end

  def logout
    reset_session
    flash[:success] = 'Logout successful'
    redirect_to root_url
  end

  private

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation)
  end
end
