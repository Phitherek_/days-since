class ApplicationController < ActionController::Base
  helper_method :current_user
  def current_user
    @current_user ||= User.find_by(id: session[:current_user_id])
  end

  def require_current_user
    if current_user.blank?
      reset_session
      flash[:error] = 'You have to login to access this area'
      redirect_to root_path
    end
  end
end
