class CollaborationsController < ApplicationController
  before_action :require_current_user, only: [:index, :create, :destroy]
  before_action :find_counter, only: [:index, :create, :destroy]
  before_action :find_collaboration, only: [:destroy]
  def index
    @collaborations = @counter.collaborations.includes(:user)
    @available_users = User.where.not(id: Array.wrap(@collaborations.pluck(:user_id)) + [current_user.id])
    @new_collaboration = @counter.collaborations.build
  end

  def create
    user = User.find(params[:collaboration][:user_id])
    @collaboration = @counter.collaborations.create(user: user)
    if @collaboration.persisted?
      flash[:success] = 'New collaborator added successfully'
    else
      flash[:error] = "Could not add new collaborator: #{@collaboration.errors.full_messages.join(', ')}"
    end
    redirect_to counter_collaborations_url(@counter)
  end

  def destroy
    if @collaboration.destroy
      flash[:success] = 'Collaborator deleted successfully'
    else
      flash[:error] = 'Could not delete collaborator'
    end
    redirect_to counter_collaborations_url(@counter)
  end

  private

  def find_counter
    @counter = current_user.counters.find(params[:counter_id])
  end

  def find_collaboration
    @collaboration = @counter.collaborations.find(params[:id])
  end
end
