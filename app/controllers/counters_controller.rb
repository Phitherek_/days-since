class CountersController < ApplicationController
  before_action :require_current_user, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :find_counter, only: [:edit, :update, :destroy]
  before_action :find_counter_for_display, only: [:show, :fullscreen]

  def index
    @counters = current_user.counters.includes(collaborations: :user)
    @collaborator_counters = current_user.collaborator_counters.includes(:user)
  end

  def new
    @counter = current_user.counters.build
  end

  def create
    @counter = current_user.counters.create(counter_params)
    if @counter.persisted?
      flash[:success] = 'Counter created successfully'
      redirect_to counters_url
    else
      flash[:error] = "Could not create counter: #{@counter.errors.full_messages.join(', ')}"
      render :new
    end
  end

  def show

  end

  def edit; end

  def update
    if @counter.update(counter_params)
      flash[:success] = 'Counter updated successfully'
      redirect_to counters_url
    else
      flash[:error] = "Could not update counter: #{@counter.errors.full_messages.join(', ')}"
      render :edit
    end
  end

  def destroy
    if @counter.destroy
      flash[:success] = 'Counter deleted successfully'
    else
      flash[:error] = 'Could not delete counter'
    end
    redirect_to counters_url
  end

  def fullscreen
    render layout: 'fullscreen'
  end

  def reset
    @counter = Counter
                 .includes(collaborations: :user)
                 .where('counters.user_id = ? OR collaborations.user_id = ?', current_user&.id, current_user&.id)
                 .references(:counters, :collaborations)
                 .find(params[:id])
    if @counter.reset
      flash[:success] = 'Counter reset successful'
    else
      flash[:error] = "Could not reset counter: #{@counter.errors.full_messages.join(', ')}"
    end
    redirect_to counters_url
  end

  private

  def find_counter
    @counter = current_user.counters.includes(collaborations: :user).find(params[:id])
  end

  def find_counter_for_display
    @counter = Counter
                 .includes(:collaborations)
                 .where('counters.user_id = ? OR collaborations.user_id = ? OR public IS TRUE', current_user&.id, current_user&.id)
                 .references(:counters, :collaborations)
                 .find_by!(slug: params[:id])
  end

  def counter_params
    params.require(:counter).permit(:name, :slug, :description, :last_date, :public)
  end
end
