import consumer from "./consumer";

let setupChannel = function() {
  let counterCount = $('.counter-count');
  let counterDescription = $('.counter-description');
  let counterRecordValue = $('.counter-record-value');

  if(counterCount.length > 0) {
    let counterId = counterCount.data('id');
    window.counterChannelSubscription = consumer.subscriptions.create({ channel: 'CounterChannel', id: counterId }, {
      received(data) {
        console.log('CounterChannel: data received: ' + JSON.stringify(data));
        if(data.action === 'update') {
          counterCount.html(data.value);
          counterDescription.html(data.description);
          counterRecordValue.html(data.record_value);
        }
      }
    });
  } else {
    if(window.counterChannelSubscription !== undefined) {
      window.counterChannelSubscription.unsubscribe();
      delete window.counterChannelSubscription;
    }
  }
};

$(function() {
  $(document).on('turbolinks:load', setupChannel);
  setupChannel();
});