$(function() {
  let initializeNewCollaboratorSelect = function() {
    $('#collaborationUserId').selectpicker();
  };
  $(document).on('turbolinks:load', initializeNewCollaboratorSelect);
  initializeNewCollaboratorSelect();
});