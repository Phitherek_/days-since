$(function() {
  let initializeLastDatePicker = function() {
    $('#counterLastDate').datetimepicker({
      format: 'Y-m-d H:i'
    });
  };
  $(document).on('turbolinks:load', initializeLastDatePicker);
  initializeLastDatePicker();
});